let navItems = document.querySelector("#nav-items")
if(!userToken){
	navItems.innerHTML =
		`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link" id="home-link"> Home </a>
		</li>

		<li class="nav-item ">
			<a href="./courses.html" class="nav-link" id="courses-link"> Courses </a>
		</li>
		<li class = "nav-item">
			<a href="./login.html" class="nav-link" id="signin-link">Sign In</a>
		</li>
		<li class = "nav-item">
			<a href="./register.html" class="nav-link" id="register-link">Register</a>
		</li>
		`
}

else if(userToken){
	navItems.innerHTML =
		`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link" id="home-link"> Home </a>
		</li>

		<li class="nav-item ">
			<a href="./courses.html" class="nav-link" id="courses-link"> Courses </a>
		</li>
		<li class = "nav-item">
			<a href="./profile.html" class="nav-link" id="profile-link">Profile</a>
		</li>
		<li class = "nav-item">
			<a href="./logout.html" class="nav-link" id="signout-link">Sign Out</a>
		</li>
		`
}



let emailUser = localStorage.getItem("email")
console.log(emailUser)

let changePasswordForm = document.querySelector("#changePassword");

changePasswordForm.addEventListener("submit", (e) => {
	let passwordOld = document.querySelector("#passwordOld").value;
	let passwordNew1 = document.querySelector("#passwordNew1").value;
	let passwordNew2 = document.querySelector("#passwordNew2").value;
		 	console.log(passwordOld)
	 	console.log(passwordNew1)
	 	console.log(passwordNew2)
		e.preventDefault();
	if (passwordOld == null || passwordNew1 == null || passwordNew2 == null) {
		alert("Empty Inputs")
	}
	else if (passwordNew1 !== passwordNew2) {
		alert('New Password didn\'t match')
	}
	 else {

	 	console.log(emailUser)
		fetch('https://peaceful-waters-74842.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${userToken}`

			},
			body: JSON.stringify({
				email: emailUser,
				password: passwordOld
			})
		})
		.then(res => {
			return res.json()
			console.log(res)
		})
		.then(data => {
			if (data.accessToken) {
				fetch('https://peaceful-waters-74842.herokuapp.com/api/users/change-password', {
					method: 'PUT',
					headers: {
						'Content-Type' : 'application/json',
						Authorization: `Bearer ${userToken}`

					},
					body: JSON.stringify({
						email: emailUser,
						password: passwordNew1
					})
				}).then(res => {
					return res.json()
				}).then(data => {
					if (data) {
						alert ('Password changed successfully')
					}
					else {
						alert('Something Went Wrong')
					}
					
				})
			}else{
				alert('Wrong Password')
			}
		})
	}
})
function toggleDisplay() {
  let x = document.querySelector("#passwordOld");
  let y = document.querySelector("#passwordNew1");
  let z = document.querySelector("#passwordNew2");
  if (x.type === "password") {
    x.type = "text";
    y.type = "text";
    z.type = "text";
  } else {
    x.type = "password";
    y.type = "password";
    z.type = "password";

  }
}