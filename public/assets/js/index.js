let navItems = document.querySelector("#nav-items")


let userToken = localStorage.getItem("token")
console.log(userToken)

if(!userToken){
	navItems.innerHTML =
		`
		<li class="nav-item">
			<a href="./index.html" class="nav-link disabled" id="home-link"> Home </a>
		</li>

		<li class="nav-item ">
			<a href="./pages/courses.html" class="nav-link"> Courses </a>
		</li>
		<li class = "nav-item">
			<a href="./pages/login.html" class="nav-link" id="signin-link">Sign In</a>
		</li>
		<li class = "nav-item">
			<a href="./pages/register.html" class="nav-link" id="register-link">Register</a>
		</li>
		`
}

else if(userToken){
	navItems.innerHTML =
		`
		<li class="nav-item">
			<a href="./pages/index.html" class="nav-link disabled" id="home-link"> Home </a>
		</li>

		<li class="nav-item ">
			<a href="./pages/courses.html" class="nav-link"> Courses </a>
		</li>
		<li class = "nav-item">
			<a href="./pages/profile.html" class="nav-link" id="profile-link">Profile</a>
		</li>
		<li class = "nav-item">
			<a href="./pages/logout.html" class="nav-link" id="signout-link">Sign Out</a>
		</li>
		`
}