let navItems = document.querySelector("#nav-items")
if(!userToken){
	navItems.innerHTML =
		`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link" id="home-link"> Home </a>
		</li>

		<li class="nav-item ">
			<a href="./courses.html" class="nav-link" id="courses-link"> Courses </a>
		</li>
		<li class = "nav-item">
			<a href="./login.html" class="nav-link" id="signin-link">Sign In</a>
		</li>
		<li class = "nav-item">
			<a href="./register.html" class="nav-link" id="register-link">Register</a>
		</li>
		`
}

else if(userToken){
	navItems.innerHTML =
		`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link" id="home-link"> Home </a>
		</li>

		<li class="nav-item ">
			<a href="./courses.html" class="nav-link" id="courses-link"> Courses </a>
		</li>
		<li class = "nav-item">
			<a href="./profile.html" class="nav-link" id="profile-link">Profile</a>
		</li>
		<li class = "nav-item">
			<a href="./logout.html" class="nav-link" id="signout-link">Sign Out</a>
		</li>
		`
}

let idUser = localStorage.getItem("id")
editForm = document.querySelector("#editProfile")

// 2. Add an event listener to the form that will add a course on submit
editForm.addEventListener("submit", (e) => {

// 3. Select the input fields
let firstName = document.querySelector("#firstName").value
let mobileNumber = document.querySelector("#mobileNumber").value
let lastName = document.querySelector("#lastName").value

// 	//prevent the page from reloading upon submitting the form
e.preventDefault();
// 4. Create a variable that will store the token of the user

// 	//retrieve the JWT stored in our local storage for auth

// 5. Create a fetch request that will add the new course to the database
// 	//if the creation of new course is successful, redirect to courses.html and give an alert message "You have created a new course!"
// 	//else, alert("Something went wrong")
console.log(firstName)
console.log(mobileNumber)
console.log(lastName)
	if (firstName == "" || mobileNumber == "" || lastName == ""){
		alert('Fill');
	} else {
		fetch('https://peaceful-waters-74842.herokuapp.com/api/users/details', {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${userToken}`
			},
			body: JSON.stringify({
				userId: idUser,
				firstName: firstName,
				lastName: lastName,
				mobileNo: mobileNumber
			})
		})
		.then(res => {
			console.log(res)
			return res.json()
		})
		.then(data => {
			console.log(data)
			//if registration is successful
			if(data === true){
				alert("You have updated your profile")
				//redirect to login page
				window.location.replace("./profile.html")
			}else{
				//error occured in registration
				alert("Something Went wrong")
			}

		})
	}


	
})