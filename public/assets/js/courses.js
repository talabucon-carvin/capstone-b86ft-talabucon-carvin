let navItems = document.querySelector("#nav-items")
if(!userToken){
	navItems.innerHTML =
		`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link" id="home-link"> Home </a>
		</li>

		<li class="nav-item ">
			<a href="./courses.html" class="nav-link disabled" id="courses-link"> Courses </a>
		</li>
		<li class = "nav-item">
			<a href="./login.html" class="nav-link" id="signin-link">Sign In</a>
		</li>
		<li class = "nav-item">
			<a href="./register.html" class="nav-link" id="register-link">Register</a>
		</li>
		`
}

else if(userToken){
	navItems.innerHTML =
		`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link" id="home-link"> Home </a>
		</li>

		<li class="nav-item ">
			<a href="./courses.html" class="nav-link disabled" id="courses-link"> Courses </a>
		</li>
		<li class = "nav-item">
			<a href="./profile.html" class="nav-link" id="profile-link">Profile</a>
		</li>
		<li class = "nav-item">
			<a href="./logout.html" class="nav-link" id="signout-link">Sign Out</a>
		</li>
		`
}

let adminUser = localStorage.getItem("isAdmin")
let idUser = localStorage.getItem("id")
let emailUser = localStorage.getItem("email")
// console.log(idUser)
// console.log(adminUser)
// console.log(emailUser)

let modalButton = document.querySelector("#adminButton")
let cardFooter;
let deleteToggle;

//if normal user
if(adminUser == "false" || !adminUser){
	modalButton.innerHTML = null

//fetch courses from API
	fetch('https://peaceful-waters-74842.herokuapp.com/api/courses')
	.then(res => res.json())
	.then(data => {
		// console.log(data)
		let courseData;
		//if empty

		if(data.length < 1) {
			courseData = "No courses available"
		} else {
			//Enroll / View details
			filterData = data.filter(course => course.isActive == true)
			courseData = filterData.map(course => {
				let enrollToggle = false
				// console.log(course.enrollees.length)
				for (let i = 0; i < course.enrollees.length; i++) {
					// console.log(course.enrollees[i].userId)
					if (idUser == course.enrollees[i].userId){
						enrollToggle = true
					
					}
				}
				//used moment to format ISO to month day year
				parsedDate = moment(course.createdOn).format('LL')

				//extra feature when enrolled
				if (!enrollToggle) {
					cardFooter = 
					`
					<div class="d-flex justify-content-around">
						<a class="col-md-6" href="./course.html?courseId=${course._id}" value={course._id}>
							<button class="col-md-12 btn btn-info text-black viewButton">
								Select Course
							</button>
						</a>
						<div class="col-md 6">Added: ${parsedDate}</div>
					</div>	
					`
				}

				else if (enrollToggle) {
					cardFooter =
					`
					<div class="d-flex justify-content-around">
						<a class="col-md-6" href="./course.html?courseId=${course._id}" value={course._id}>
							<button class="col-md-12 btn btn-secondary">
								View Details
							</button>
						</a>
						<div class="col-md 6">Added:  ${parsedDate}</div>
					</div>			
					`
				}

				return(
					`
					<div class="col-md-6 my-3">
						<div class="card" style="background-color: #1e4258">
							<div class="card-body">
								<h5 class="card-title">${course.name}</h5>
								<p class="card-text text-left">
									${course.description}
								</p>
								<p class="card-text text-right">
									₱${course.price.toLocaleString()}
								</p>
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
						</div>	
					</div>

					`
					)
			}).join("")
			//since the collection is an array, we can use join to indicate the separator 
			let container = document.querySelector("#coursesContainer")
			container.innerHTML = courseData
		}
	})
} else if (adminUser == "true") {
//if admin
//fetch courses from API
	fetch('https://peaceful-waters-74842.herokuapp.com/api/courses')
	.then(res => res.json())
	.then(data => {
		let courseData;
		if(data.length < 1) {
			courseData = "No courses available"
		} else {

			//if the delete button is clicked, the course will be disabled
			//key = courseId
			//value = the element's courseId
			//else iterate the courses collection and display each course
			courseData = data.map(course => {
			//if the user is a regular user, display when the course was created and display the button, select course

			//else, if the user is admin, display the edit and delete button
			//when the edited button is clicke dwit will direct the user to editCourse.html(name ofbutton edit/update)
				
				if(course.isActive == false){
					opacity = `0.3`
					courseAvailabilityToggle = 
						`
						<a href="./activateCourse.html?courseId=${course._id}" value={course._id} class="text-black editButton">
							<button class="btn btn-warning text-dark col-md-3 my-1 editButton""">
								Activate
							</button>
						</a>
						`
				}
				else if (course.isActive == true) {
					opacity = `1`
					courseAvailabilityToggle = 
						`
						<a href="./deleteCourse.html?courseId=${course._id}" value={course._id} class="text-black editButton disabled">
							<button class="btn btn-danger text-white col-md-3 my-1 editButton""">
								Disable
							</button>
						</a>
						`
				}
				cardFooter = 
				`
				<a href="./course.html?courseId=${course._id}" value={course._id} class="text-white">
					<button class="btn btn-primary col-md-3 my-1 editButton""">
						Details
					</button>
				</a>
				<a href="./editCourse.html?courseId=${course._id}" value={course._id} class="text-white">
					<button class="btn btn-info text-white col-md-3 my-1 editButton"">
						Edit
					</button>
				</a>
				${courseAvailabilityToggle}
						
				`
				return(
					`
					<div class="col-md-6 my-3">
						<div class="card" style="background-color: rgba(30, 66, 88,${opacity})">
							<div class="card-body">
								<h5 class="card-title">${course.name}</h5>
								<p class="card-text text-left">
									${course.description}
								</p>
								<p class="card-text text-right">
									₱${course.price.toLocaleString()}
								</p>
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
						</div>	
					</div>

					`
				)
			}).join("")
			//since the collection is an array, we can use join to indicate the separator 
			let container = document.querySelector("#coursesContainer")
			container.innerHTML = courseData
			modalButton.innerHTML = 
				`
				<div class = "col-md-2 offset-md-10">
					<a href="./addCourse.html" class="btn btn-block btn-primary">Add Course</a>
				</div>
				`
		}
	})
}
