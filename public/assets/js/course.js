//navbar conditional rendering
let navItems = document.querySelector("#nav-items")
if(!userToken){
	navItems.innerHTML =
		`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link" id="home-link"> Home </a>
		</li>

		<li class="nav-item ">
			<a href="./courses.html" class="nav-link" id="courses-link"> Courses </a>
		</li>
		<li class = "nav-item">
			<a href="./login.html" class="nav-link" id="signin-link">Sign In</a>
		</li>
		<li class = "nav-item">
			<a href="./register.html" class="nav-link" id="register-link">Register</a>
		</li>
		`
}

else if(userToken){
	navItems.innerHTML =
		`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link" id="home-link"> Home </a>
		</li>

		<li class="nav-item ">
			<a href="./courses.html" class="nav-link" id="courses-link"> Courses </a>
		</li>
		<li class = "nav-item">
			<a href="./profile.html" class="nav-link" id="profile-link">Profile</a>
		</li>
		<li class = "nav-item">
			<a href="./logout.html" class="nav-link" id="signout-link">Sign Out</a>
		</li>
		`
}
// ================course.js=================
// Add the logic that will retrieve the details of an individual course and display the information, when clicking on the "Select Course" button.
// 1. Create a variable that will store all the parameters/data passed via the url
// 	//window.location.search returns the query string part of the URL
	// console.log(window.location.search)

// 	//instantiate a URLSearchParams object so we can execute methods to access specific parts of the query string
// 	//this is for getting the specific courseId from the courses.html
let params = new URLSearchParams(window.location.search);

// 2. Create a variable that will store the courseId retrieved from the URL
// 	//Spread the values to set the key-value pairs of the object URLSearchParams
	// console.log(...courseId);

// 	//the method checks if the courseId key exists in the URL query string
// 	//true means that the key exists
	// console.log(params.get('courseId'))

// 	//get method returns the value of the key passed in as an argument
let courseId = params.get('courseId')
console.log(courseId)
// 3. Create a variable to store the token of the registered user
// 	//retrieve the JWT stored in our local storage for auth

let courseData
// 4. Create the variables and store the document objects that will be used to render all the data retrieved from the course
let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let courseDetails = document.querySelector("#courseDetails");

let adminUser = localStorage.getItem("isAdmin")
let userId = localStorage.getItem("id")

if(adminUser == "false" || !adminUser){
// 5. Create a fetch request that will get the data of an individual course
// URL${course._id}
	fetch(`https://peaceful-waters-74842.herokuapp.com/api/courses/${courseId}`)
	.then(res => res.json())
	// console.log(res)
	.then(data => {
	console.log(data)
	let toggle = false
	for (let i = 0; i < data.enrollees.length; i++) {
		if (data.enrollees[i].userId == userId)
		toggle = true 
	}
	if (!toggle) {
	// console.log(data)
		courseData =
		`
		<div class="container my-5 text-center">	
			<div class="row">		
				<div class="col-md-6 offset-md-3">
					<section class="jumbotron" style="background-color: #1e4258">
						<h2 id="courseName" class="mt-5">Welcome to Zuitter!</h2>
						<p id="courseDesc" class="lead"></p>
						<p id="courseDetails"></p>
						<p class="secondary">
							Price: ₱ <span id="coursePrice"></span> 
						</p>
						
						<span id="enrollContainer" class="btn btn-primary" type="submit">
							Enroll
						</span>
					</section>
					
				</div>
			</div>

		</div>
		`
		let courseDetailsContainer = document.querySelector("#courseDetailsContainer")
		courseDetailsContainer.innerHTML = courseData
		let courseName = document.querySelector("#courseName")
		let courseDesc = document.querySelector("#courseDesc")
		let coursePrice = document.querySelector("#coursePrice")
		let courseDetails = document.querySelector("#courseDetails");
		courseName.innerHTML = data.name;
		courseDesc.innerHTML = data.description;
		coursePrice.innerHTML = data.price;
		courseDetails.innerHTML = data.details;
		let enroll = document.querySelector("#enrollContainer");
		enroll.addEventListener("click", (e) => {
			e.preventDefault()
			if(userToken == null) {
				alert('Please Log in');
				window.location.replace("./login.html")
			}
			else if (courseId == null) {
				alert('Select a Course')
			}
			else {
				fetch('https://peaceful-waters-74842.herokuapp.com/api/users/enroll', {
					method:'POST',
					headers: {
						'Content-Type': 'application/jSON',
						Authorization: `Bearer ${userToken}`
					},	
					body: JSON.stringify({
						userId: userId,
						courseId: courseId
					})
				})
				.then(res => res.json())
				
				.then(data => {
					// console.log(data)
					if(data) {
						alert('Thank you! for enrolling. See you!')
						window.location.replace("./courses.html")
					} 
					else {
						alert('Something went wrong')
					}
				})
			}
		})
	} else {
		courseData =
		`
		<div class="container my-5 text-center">	
			<div class="row">		
				<div class="col-md-6 offset-md-3">
					<section class="jumbotron" style="background-color: #1e4258">
						<h2 id="courseName" class="mt-5">Welcome to Zuitter!</h2>
						<p id="courseDesc" class="lead"></p>
						<p id="courseDetails"></p>					
						<p class="secondary">
							Price: ₱ <span id="coursePrice"></span> 
						</p>
						<span id="enrollContainer" class="btn btn-primary disabled">
							Enrolled
						</span>
						
					</section>
					
				</div>
			</div>

		</div>
		`
		let courseDetailsContainer = document.querySelector("#courseDetailsContainer")
		courseDetailsContainer.innerHTML = courseData
		let courseName = document.querySelector("#courseName")
		let courseDesc = document.querySelector("#courseDesc")
		let coursePrice = document.querySelector("#coursePrice")
		let courseDetails = document.querySelector("#courseDetails");
		courseName.innerHTML = data.name;
		courseDesc.innerHTML = data.description;
		coursePrice.innerHTML = data.price.toLocaleString();
		courseDetails.innerHTML = data.details
		}

	})

}
//if admin
else if (adminUser == "true") {
	let courseDetails = []
	console.log(courseId)
//fetch course details
	fetch(`https://peaceful-waters-74842.herokuapp.com/api/courses/${courseId}`)
	.then(res => res.json())
	
	.then(data => {
	console.log(data)
	let parsedDate = moment(data.createdOn).format('LL')
	courseData =

	`
	<div class="container">
		<div class="col-md-12 my-3">
			<h3 class="text-center">Course Details</h3>
		</div>
		<div class="col-md-12 my-3">
			<table class="table table-dark table-striped">
				<thead>
				    <tr>
				        <th>Course Name</th>
				        <th>Description</th>
				        <th>Price</th>
				        <th>Date Added</th>
					        
				    </tr>
				</thead>
				<tbody>
				    <tr>
				        <td id="courseName"></td>
				        <td id="courseDesc"></td>
				        <td id="coursePrice"></td>
				       	<td>${parsedDate}</td>

		     		</tr>

		     	</tbody>
		     	<thead>
		     		<tr>
		     			<th colspan="4" class="text-center">Details</th>
		     		</tr
		     	</thead>
		     	<tbody>
		     		<tr>
		     			<td id="courseDetails" colspan="4"></td>
		     		</tr		     		
		     	</tbody>
		    </table>
		</div>
	    <div class="col-md-12 my-3">
			<h3 class="text-center">Enrollees</h3>
		</div>
		<div class="col-md-12 my-3">
			<table class="table table-dark table-striped">
				<thead>
				    <tr>
				        <th>First Name</th>
				        <th>Last Name</th>
				        <th>Email</th>
				        <th>Mobile Number</th>
					</tr>
				</thead>
				<tbody id="enrolleesDataContainer">
				</tbody>
			</table>
		</div>
   	</div>
	`
	let courseDetailsContainer = document.querySelector("#courseDetailsContainer")
	courseDetailsContainer.innerHTML = courseData
	let courseName = document.querySelector("#courseName")
	let courseDesc = document.querySelector("#courseDesc")
	let coursePrice = document.querySelector("#coursePrice")
	let courseDetails = document.querySelector("#courseDetails");

	courseName.innerHTML = data.name;
	courseDesc.innerHTML = data.description;
	coursePrice.innerHTML = '₱ ' + data.price.toLocaleString();
	courseDetails.innerHTML = data.details;


	let userId
	let fetchArray = [];
		//iterate fetch enrollees accesstoken
		for (let i = 0; i < data.enrollees.length; i++) {
			userId = data.enrollees[i].userId;
			fetchArray.push(fetch('https://peaceful-waters-74842.herokuapp.com/api/users/details', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${userToken}`
				},
				body: JSON.stringify({
					userId: `${userId}`
					})
			}))	

		}
		console.log(fetchArray)
		//waits for fetch loop to finish
		Promise.all(fetchArray)
		.then(responses => {
		// console.log(responses)
		
		//waits for resoponses to convert into array of json
		Promise.all(responses.map(res => 
		res.json()
			))	
		
			.then (data => {
				let fetchArray = [];
				// console.log(data)
			
					for (let i = 0; i < data.length; i++) {
					let fetchedToken = data[i].accessToken
					fetchArray.push(fetch('https://peaceful-waters-74842.herokuapp.com/api/users/details', {
						headers: {
							'Content-Type': 'application/json',
							Authorization: `Bearer ${fetchedToken}`
						}
					}))
					}
					//waits for loop to finish
					// console.log(fetchArray)
					Promise.all(fetchArray)
					.then(responses => {
					//waits for resoonse to convert
					// console.log(responses)
					Promise.all(responses.map(res => 
					res.json()
						))	
		
					.then (data => {
						console.log(data)
						//final step of asyc chain
						//insert view modification
						let enrolleesDataContainer = document.querySelector("#enrolleesDataContainer")
						
						enrolleesData = data.map(data => {
						return(
						`
				        <tr>
					        <td>${data.firstName}</td>
					        <td>${data.lastName}</td>
					        <td>${data.email}</td>
					        <td>0${data.mobileNo}</td>
			     		 </tr>

						`
						)
						}).join("")
						//since the collection is an array, we can use join to indicate the separator 
						
						enrolleesDataContainer.innerHTML = enrolleesData


					})
				})
			})
		})
	})
}