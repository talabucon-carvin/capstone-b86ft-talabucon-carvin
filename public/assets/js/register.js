let navItems = document.querySelector("#nav-items")
if(!userToken){
	navItems.innerHTML =
		`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link" id="home-link"> Home </a>
		</li>

		<li class="nav-item ">
			<a href="./courses.html" class="nav-link" id="courses-link"> Courses </a>
		</li>
		<li class = "nav-item">
			<a href="./login.html" class="nav-link" id="signin-link">Sign In</a>
		</li>
		<li class = "nav-item">
			<a href="./register.html" class="nav-link disabled" id="register-link">Register</a>
		</li>
		`
}

else if(userToken){
	navItems.innerHTML =
		`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link" id="home-link"> Home </a>
		</li>

		<li class="nav-item ">
			<a href="./courses.html" class="nav-link" id="courses-link"> Courses </a>
		</li>
		<li class = "nav-item">
			<a href="./profile.html" class="nav-link disabled" id="profile-link">Profile</a>
		</li>
		<li class = "nav-item">
			<a href="./logout.html" class="nav-link" id="signout-link">Sign Out</a>
		</li>
		`
}
// const form = document.getElementById("registerUser")
// form.addEventListener("submit", submitted)
// function submitted(event){
// 	console.log("submitted");
// 	event.preventDefault();
// }

// const formElements = document.getElementsByClassName("form-control")


let registerForm = document.querySelector("#registerUser")
registerForm.addEventListener("submit", (e) => {
	e.preventDefault()
	let firstName = document.querySelector("#firstName").value;
	let lastName = document.querySelector("#lastName").value;
	let mobileNumber = document.querySelector("#mobileNumber").value;
	let userEmail = document.querySelector("#userEmail").value;
	let password1 = document.querySelector("#password1").value;
	let password2 = document.querySelector("#password2").value;
	let registerUser = document.querySelector("#registerUser").value;
	//validation to enable submit button when all fields are populated and both passwords match
	//mobile  number is = 11
	// if(password1 === password2 || mobileNumber.toString().length === 11){
	// 	console.log(1)
	// 	e.preventDefault()
	// }
	console.log(password1)
	console.log(password2)
	if((password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNumber.length === 11)) {
		
	//check the database
	//check for duplicate email in database first
	//url- where we can get the duplicate routes in our server
	//asynchronously load contents of the URL
	fetch('https://peaceful-waters-74842.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				"email": userEmail
			})
		})
		//return a promise that resolves when the result is loaded
		.then(res => res.json())//call this junction when the result is loaded
		.then(data => {
			//if no duplicates found
			//get the route for registration in our server
			if(data === true) {
			alert('Email already used')
			}



			else if (data === false) {
				fetch('https://peaceful-waters-74842.herokuapp.com/api/users/', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						"firstName": firstName,
						"lastName": lastName,
						"email": userEmail,
						"password": password1,
						"mobileNo": mobileNumber
					})
				})
				.then(res => {
					return res.json()
				})
				.then(data => {
					console.log(data)
					//if registration is successful
					if(data === true){
						alert("Registered Successfully")
						//redirect to login page
						window.location.replace("./login.html")
					}else{
						//error occured in registration
						alert("Something Went wrong")
					}

				})
			}


		})
	}else{
		alert("Something Went Wrong, Please try again")
	}


});

function toggleDisplay() {
  let x = document.querySelector("#password1");
  let y = document.querySelector("#password2");
  if (x.type === "password") {
    x.type = "text";
    y.type = "text";
  } else {
    x.type = "password";
    y.type = "password";
  }
}