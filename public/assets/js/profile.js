//nav bar conditional render
let navItems = document.querySelector("#nav-items")
if(!userToken){
	navItems.innerHTML =
		`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link" id="home-link"> Home </a>
		</li>

		<li class="nav-item ">
			<a href="./courses.html" class="nav-link" id="courses-link"> Courses </a>
		</li>
		<li class = "nav-item">
			<a href="./login.html" class="nav-link disabled" id="signin-link">Sign In</a>
		</li>
		<li class = "nav-item">
			<a href="./register.html" class="nav-link" id="register-link">Register</a>
		</li>
		`
}

else if(userToken){
	navItems.innerHTML =
		`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link" id="home-link"> Home </a>
		</li>

		<li class="nav-item ">
			<a href="./courses.html" class="nav-link" id="courses-link"> Courses </a>
		</li>
		<li class = "nav-item">
			<a href="./profile.html" class="nav-link disabled" id="profile-link">Profile</a>
		</li>
		<li class = "nav-item">
			<a href="./logout.html" class="nav-link" id="signout-link">Sign Out</a>
		</li>
		`
}
//store fetched data
let userDetails = []; //store user details
let userData; //store html value
let fetchArray = []; //store fetch array
let userEnrollments = []; //store user enrolmment details

if (userToken) {
	//successful authentication will return JSON web token
	fetch('https://peaceful-waters-74842.herokuapp.com/api/users/details', {
		headers: {
			Authorization: `Bearer ${userToken}`
			}
		})
	.then(res => {
		return res.json()
	})
	.then(data => {
	userDetails = data
	//get value of courseId from user enrollments
	//store in array, iterate courses enrolled, 
	let courseId
	for (let i = 0; i < data.enrollments.length; i++ ) {
			courseId = data.enrollments[i].courseId;
			fetchArray.push(fetch(`https://peaceful-waters-74842.herokuapp.com/api/courses/${courseId}`))

	}
	console.log(fetchArray)
	//promise all to wait the loop to finish	
	Promise.all(fetchArray)
	.then(responses => {
	//promise all to wait responses to convert to json
		Promise.all(responses.map(res => 
			res.json()
			))	

		.then (data => {
		userEnrollments = data;
		console.log(userDetails)
		console.log(userEnrollments)
		//final process of asynchronous chain
		//insert view modification here
		let enrollments
		//iterate all data to table

		enrollments = userEnrollments.map(userEnrollments => {
			//used moment to convert ISO formatted date to month day year
			parsedDate = moment(userEnrollments.createdOn).format('LL')
			return(
			`
		        <tr>
			        <td>${userEnrollments.name}</td>
			        <td>${userEnrollments.description}</td>
			        <td>₱${userEnrollments.price.toLocaleString()}</td>
			        <td>${parsedDate}</td>
	     		 </tr>

			`
			)
		}).join("")		
		userData = 
		`
		<div class="col-md-12 my-3">
			<h3 class="text-center">Profile</h3>
		</div>
		<div class="d-flex justify-content-end col-md-12 ">
		
			<a class="mx-3" href="./editProfile.html">
				<button class="btn btn-info text-light" id="editProfileButton">Edit Profile</button>
			</a>
		
		
			<a href="./changePassword.html">
				<button class="btn btn-danger text-light" id="editProfileButton">Change Password</button>
			</a>
		
		</div>
		<div class="col-md-12 my-3">
			<table class="table table-dark table-striped">
			    <thead>
			      	<tr>
				        <th>Firstname</th>
				        <th>Lastname</th>
				        <th>Email</th>
				        <th>Mobile Number</th>
				        
			      	</tr>
			    </thead>
			    <tbody>
			         <tr>
				        <td>${userDetails.firstName}</td>
				        <td>${userDetails.lastName}</td>
				        <td>${userDetails.email}</td>
				        <td>0${userDetails.mobileNo}</td>

	     			 </tr>
	     		</tbody>
	     	</table>
     	</div>
		<div class="col-md-12 my-3">
			<h3 class="card-header text-center">Courses</h3>
		</div>
		<div class="col-md-12 my-3">
			<table class="table table-dark table-striped">
			    <thead>
			      	<tr>
				        <th>Course Name</th>
				        <th>Description</th>
				        <th>Price</th>
				        <th>Date Created</th>
				    </tr>
			    </thead>
			    <tbody>
			    	${enrollments}
	     		</tbody>
	     	</table>
     	</div>


		`
		let profileContainer = document.querySelector("#profileContainer")
		profileContainer.innerHTML = userData
		})
	})

})	
// <div class="col-md-12 my-3">
// 			<div class="card" style="background-color: #1e4258">
// 				<div class="card-body">
// 					<h5 class="card-title">First Name: ${userDetails.firstName}</h5>
// 					<p class="card-text">
// 						Last Name: ${userDetails.lastName}
// 					</p>
// 					<p class="card-text">
// 						Email: ${userDetails.email}
// 					</p>
// 				</div>
// 				<div class="card-footer">
// 					Mobile No.: ${userDetails.mobileNo}
// 					<span><a href="./editProfile.html">Edit Profile</a></span>
// 				</div>
// 			</div>	
// 		</div>


	


	

}else {
	alert("Authentication failure")
}