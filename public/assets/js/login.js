let navItems = document.querySelector("#nav-items")
if(!userToken){
	navItems.innerHTML =
		`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link" id="home-link"> Home </a>
		</li>

		<li class="nav-item ">
			<a href="./courses.html" class="nav-link" id="courses-link"> Courses </a>
		</li>
		<li class = "nav-item">
			<a href="./login.html" class="nav-link disabled" id="signin-link">Sign In</a>
		</li>
		<li class = "nav-item">
			<a href="./register.html" class="nav-link" id="register-link">Register</a>
		</li>
		`
}

else if(userToken){
	navItems.innerHTML =
		`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link" id="home-link"> Home </a>
		</li>

		<li class="nav-item ">
			<a href="./courses.html" class="nav-link" id="courses-link"> Courses </a>
		</li>
		<li class = "nav-item">
			<a href="./profile.html" class="nav-link disabled" id="profile-link">Profile</a>
		</li>
		<li class = "nav-item">
			<a href="./logout.html" class="nav-link" id="signout-link">Sign Out</a>
		</li>
		`
}

let loginForm = document.querySelector("#logInUser")
loginForm.addEventListener("submit", (e) => {
	e.preventDefault();
	let email = document.querySelector("#userEmail").value;
	let password = document.querySelector("#password").value;
	if (email === null || password == "" ){
		alert("please input your email and/or password")
	} else {

		fetch('https://peaceful-waters-74842.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res =>{
			return res.json()
		})
		.then(data => {
			if (data.accessToken) {
				//successful authentication will return JSON web token
				localStorage.setItem('token', data.accessToken);

				fetch('https://peaceful-waters-74842.herokuapp.com/api/users/details', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
						}
					})
				.then(res => {
					return res.json()
				})
				.then(data => {
				//set the global user state to have properties containing authenticated user's ID and role
					localStorage.setItem("id", data._id)
					localStorage.setItem("isAdmin", data.isAdmin)
					localStorage.setItem("email", data.email)
					// console.log(data)
					window.location.replace("./courses.html")
				})
			}else {
				alert("Authentication failure")
			}
		})
	}
})

function toggleDisplay() {
  let x = document.querySelector("#password");

  if (x.type === "password") {
   x.type = "text";
  } else {
   x.type = "password";
  }
}			

		
