// ============================deleteCourse.js=============================
// Create the delete course function work
// 1. Get the couseId from the URL
// 	//window.location.search returns the query string part of the URL
// 	//console.log(window.location.search)
let userToken = localStorage.getItem("token")
// 	//instantiate a URLSearchParams object so we can execute methods to access specific parts of the query string
// 	//this is for getting the specific courseId from the courses.html
console.log(userToken)
let params = new URLSearchParams(window.location.search);

// 	Create a variable that will store the courseId retrieved from the URL
// 		//Spread the values to set the key-value pairs of the object URLSearchParams
// 		//console.log(...courseId);

// 		//the method checks if the courseId key exists in the URL query string
// 		//true means that the key exists
// 		//console.log(params.get('courseId'))

// 		//get method returns the value of the key passed in as an argument

let courseId = params.get('courseId')
// 		console.log(courseId)
// 2. Get the token of the user from the local storage
// 3. Create a fetch request to delete the specific course
fetch(`https://peaceful-waters-74842.herokuapp.com/api/courses/${courseId}`, {
	method: 'DELETE',
	headers: {
		Authorization: `Bearer ${userToken}`
	}
})

.then(res => res.json())
// console.log(res)
.then(data => {
window.location.replace("./courses.html")
})
// 	//Authorization is needed
// 4. Create a logic to redirect the user back to the courses page on success and alert the user if there's an error