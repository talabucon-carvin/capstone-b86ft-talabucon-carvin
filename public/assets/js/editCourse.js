let navItems = document.querySelector("#nav-items")
if(!userToken){
	navItems.innerHTML =
		`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link" id="home-link"> Home </a>
		</li>

		<li class="nav-item ">
			<a href="./courses.html" class="nav-link" id="courses-link"> Courses </a>
		</li>
		<li class = "nav-item">
			<a href="./login.html" class="nav-link" id="signin-link">Sign In</a>
		</li>
		<li class = "nav-item">
			<a href="./register.html" class="nav-link" id="register-link">Register</a>
		</li>
		`
}

else if(userToken){
	navItems.innerHTML =
		`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link" id="home-link"> Home </a>
		</li>

		<li class="nav-item ">
			<a href="./courses.html" class="nav-link" id="courses-link"> Courses </a>
		</li>
		<li class = "nav-item">
			<a href="./profile.html" class="nav-link" id="profile-link">Profile</a>
		</li>
		<li class = "nav-item">
			<a href="./logout.html" class="nav-link" id="signout-link">Sign Out</a>
		</li>
		`
}


// Create the edit course function work
// 1. Get the couseId from the URL
// 	//window.location.search returns the query string part of the URL
// 	//console.log(window.location.search)
// 	//instantiate a URLSearchParams object so we can execute methods to access specific parts of the query string
// 	//this is for getting the specific courseId from the courses.html
let params = new URLSearchParams(window.location.search);
// 	Create a variable that will store the courseId retrieved from the URL
// 		//Spread the values to set the key-value pairs of the object URLSearchParams
// 		//console.log(...courseId);

// 		//the method checks if the courseId key exists in the URL query string
// 		//true means that the key exists
// 		//console.log(params.get('courseId'))

// 		//get method returns the value of the key passed in as an argument
let courseId = params.get('courseId')
// 		console.log(courseId)
// 2. Select the form input fields
let editForm = document.querySelector("#editCourse")
editForm.addEventListener("submit", (e) => {
let courseName = document.querySelector("#courseName").value
let coursePrice = document.querySelector("#coursePrice").value
let courseDescription = document.querySelector("#courseDescription").value
let courseDetails = document.querySelector("#courseDetails").value
e.preventDefault();
// 3. Create a fetch request that will get the details of a specific course
	if (courseName == "" || coursePrice == "" || courseDescription == "" || courseDetails == ""){
		alert('Fill');
	} else {
		fetch('https://peaceful-waters-74842.herokuapp.com/api/courses', {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${userToken}`
			},
			body: JSON.stringify({
				"_id": courseId,
				"name": courseName,
				"description": courseDescription,
				"price": coursePrice,
				"details": courseDetails
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			console.log(data)
			//if registration is successful
			if(data != undefined){
				alert("You have updated a course")
				//redirect to login page
				window.location.replace("./courses.html")
			}else{
				//error occured in registration
				alert("Something Went wrong")
			}

		})
	}
})
			
// 4. Assign the retrieved data values as placeholders and the current values of the formSubmit
// 	//assign the current values as placeholders
// 	//sample:
// 	name.placeholder = data.name
// 5. Add an event listener to the form
// 6. Create a variables that will store the details from the form and a variable to store the token of the user
// 7. Create a fetch request that will edit the course
// 8. Create a logic to redirect the user back to the courses page on success and alert the user if there's an error
